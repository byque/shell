#!/bin/bash

# Declaración formal
function minutos_a_milisegundos() {
  local minutos=$1
  # La expansión aritmética solo acepta enteros
  local ms=$(($minutos * 60000))
  echo $ms

  # Por defecto devuelve el estado de la salida del último comando ejecutado
  # en la función, pero puede asignarse cualquier valor entero de 8-bits.
  # Este valor se asigna a la variable $?
  return 123
}

# Declaración simplificada
horas_a_milisegundos() {
  echo $(($1 * 3600000))
}
