#!/bin/bash
###########################################################
# Script de aviso cuando la batería está cargada
# Está pensado para usarse como un cron job cada media hora
#   crontab -e
#   */30 * * * * /path/aviso_bateria_cargada.sh
###########################################################

BATTINFO=$(acpi -b)

if [[ $(echo $BATTINFO | grep Full) ]]; then
  DISPLAY=:0.0 /usr/bin/notify-send "Batería cargada" "$BATTINFO"

  # Si estás usando i3 es probable que quieras terminar dunst después de la
  # notificación. Causa problemas con el atajo Ctrl+`
  # sleep 5 && pkill dunst
fi
