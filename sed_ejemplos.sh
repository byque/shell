#!/bin/bash
# Ejemplos del uso de sed (Stream EDitor)

# Reemplazar el texto "delante" por "detras".
echo "delante" | sed 's/delante/detras/'

# Elegir el número de línea en la que reemplazar.
echo "detras" | sed '1s/detras/delante/'

# Buscar en un archivo las líneas que sean iguales a la expresión regular. La
# opción "n" evita la auto impresión en la terminal.
sed -n '/mi_regex/p' mi_archivo.txt

# Buscar en un archivo las líneas que NO sean iguales a la regex.
sed -n '/mi_regex/!p' mi_archivo.txt
