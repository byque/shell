#!/bin/bash
######################################################
# Script de aviso cuando la batería está baja
# Está pensado para usarse como un cron job cada 5 min
#   crontab -e
#   */5 * * * * /path/aviso_bateria_baja.sh
######################################################

BATTINFO=$(acpi -b)

if [[ $(echo $BATTINFO | grep Discharging) && \
      $(echo $BATTINFO | cut -f 5 -d " ") < 00:15:00 ]]; then
  DISPLAY=:0.0 /usr/bin/notify-send "Batería baja" "$BATTINFO"

  # Si estás usando i3 es probable que quieras terminar dunst después de la
  # notificación. Causa problemas con el atajo Ctrl+`
  # sleep 5 && pkill dunst
fi
