#!/bin/bash

# Verificar que solo raíz puede ejecutar el guión usando el identificador de
# usuario efectivo (Effective User ID)
if [[ $EUID -ne 0 ]]; then
   echo "Este guión debe ser ejecutado como raíz"
   exit 1
fi

echo "Ejecutado como raíz"
