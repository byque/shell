#!/bin/bash
 source $1/externos/assert.sh

# Se espera que `bucle_for.sh 3` escriba "a", "e", "i", "o", "u", "0", "1" y
# "2" en stdout en diferentes líneas
assert "$1/bucle_for.sh 3" "a\ne\ni\no\nu\n0\n1\n2"

# fin del paquete de pruebas
assert_end bucle_for
