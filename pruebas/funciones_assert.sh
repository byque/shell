#!/bin/bash
source $1/externos/assert.sh
source $1/funciones/funciones.sh

# Se espera que 'minutos_a_milisegundos' imprima el valor en milisegundos
assert "minutos_a_milisegundos 2" "120000"
# Se espera que 'minutos_a_milisegundos' devuelva 123 al salir
assert_raises "minutos_a_milisegundos 5" 123

# Se espera que 'horas_a_milisegundos' devuelva el valor en milisegundos
assert "horas_a_milisegundos 3" "10800000"
# Se espera que 'horas_a_milisegundos' devuelva 0 al salir
assert_raises "horas_a_milisegundos 7"

# fin del paquete de pruebas
assert_end funciones
