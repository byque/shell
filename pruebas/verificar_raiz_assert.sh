#!/bin/bash
source $1/externos/assert.sh

# Creación de usuario de systema temporal
usuario='usrtemporal'
useradd --system $usuario

# Se espera que `verificar_raiz.sh` notifique al usuario que el guion debe ser
# ejecutado como raíz y que el código de salida sea 1
assert "runuser -u $usuario $1/guiones/verificar_raiz.sh" \
  "Este guión debe ser ejecutado como raíz"
assert_raises "runuser -u $usuario $1/guiones/verificar_raiz.sh" 1

# Se espera que `verificar_raiz.sh` imprima una notificación para raíz y que
# el código de salida sea 0
assert "$1/guiones/verificar_raiz.sh" \
  "Ejecutado como raíz"
assert_raises "$1/guiones/verificar_raiz.sh"

# fin del paquete de pruebas
deluser $usuario
assert_end verificar_raiz
